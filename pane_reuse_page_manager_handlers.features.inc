<?php

/**
 * Implements hook_features_export_options().
 */
function pane_reuse_page_manager_handlers_features_export_options() {
  $options = array();

  $list = page_manager_page_manager_handlers_list();
  foreach(module_invoke_all('pane_reuse_page_manager_handlers') as $name => $value) {

    list($source_handler_name, $handler, $get_panes_by_pos, $trans_panes_by_pos) = $value;

    if ($title = $handler->conf['title']) {
      $list[$handler->name] = check_plain("$handler->task: $title ($handler->name)");
    }
    else {
      $list[$handler->name] = check_plain("$handler->task: ($handler->name)");
    }

    $options[$handler->name] = $list[$handler->name];
  }

  return $options;
}

/**
 * Implements hook_features_export().
 */
function pane_reuse_page_manager_handlers_features_export($data, &$export, $module_name = '') {

  $export['dependencies']['pane_reuse'] = 'pane_reuse';

  foreach ($data as $handler_names) {
    $export['features']['pane_reuse_page_manager_handlers'][$handler_names] = $handler_names;
  }

  // At the moment pane reuse needs at least one source page_manager handler.
  // Use pipe to add those source handlers without transformed ones
  $pipe = array();
  $list = page_manager_page_manager_handlers_list();
  foreach(array_diff_key($list, module_invoke_all('pane_reuse_page_manager_handlers')) as $name => $value) {
    $pipe['page_manager_handlers'][$name] = $name;
  }

  return $pipe;
}

/**
 * Implements hook_features_export_render().
 *
 * Use cases : UI created - db exists, no file; Default - no db, file exists;
 * Override db,file exist; added|copied - no db, no file, need feature export
 */
function pane_reuse_page_manager_handlers_features_export_render($module, $data) {
  $code = array();

  $code[] = '  $export = array();';
  $code[] = '';

  $transforms = module_invoke_all('pane_reuse_page_manager_handlers');
  $defaults = module_invoke_all_page_handlers('default_page_manager_handlers');

  // Init $handlers with $defaults
  $handlers = $defaults;
  foreach ($data as $name) {

    // Get from db or file
    $handler = page_manager_export_task_handler_load($name);

    if($handler_exists = !empty($handler)) {

      // Override handlers dont have ->conf[display] when calling page_manager_export_task_handler_load
      $overrides = empty($handler->conf['display']) && !empty($handler->conf['did']);

      // Ensure we have all handler elements by calling the export function
      $eval = page_manager_export_task_handler($handler);
      eval($eval);

      // Use the $task_handler to check if there are difference (including transforming? including different pane names).
      $task_handler = page_manager_load_task_handler($handler->task, $handler->subtask, $name);
      if ($function = ctools_plugin_load_function('page_manager', 'task_handlers', $task_handler->handler, 'clone')) {
        $function($task_handler);
      }

      $handlers[$name] = $handler;
    }
    else {
      $overrides = FALSE;

      // New handler by copy
      //TODO verify: Source handler $source_handler_name must be before $name i.e. use weight
      $defaults_handler = clone $handlers[$source_handler_name];
      $defaults[$name] = $defaults_handler;
    }

    // If there is transforming to do. Transform is always on defaults
    if(isset($transforms[$name]) && isset($defaults[$name])) {

      list($source_handler_name, $transform, $get_panes_by_pos, $trans_panes_by_pos) = $transforms[$name];

      $transformed_handler = $defaults[$name];
      pane_reuse_element_walk_recursive(
        $transform,
        function($item, $key, &$context){},
        function($item, $key, &$context){},
        // leaf callback function
        function($item, $key, &$context) use ($transformed_handler){

          $count_result = 0;
          if(is_array($item) || is_object($item)) {
            foreach($item as $none) {
              $count_result++;
            }
          }
          // Reccurse stop condition
          if($leaf_stop_cond = !$count_result) {
            $current_path = $context['current_path'];

            pane_reuse_element_ref_by_parents($transformed_handler, explode('/', $current_path), $item, FALSE, TRUE);

          }

          return $leaf_stop_cond;
        },
        $context
      );

      //Clone also display
      if(!$handler_exists) {
        $defaults_handler->conf['display'] = clone $defaults[$source_handler_name]->conf['display'];
      }

      // Doing transformation on defaults
      pane_reuse_trans_panes_by_pos($defaults, $name, $get_panes_by_pos, $trans_panes_by_pos);
    }

    $code_handler = page_manager_export_task_handler($overrides ? $handlers[$name] : $defaults[$name], $indent = '  ');
    $code[] = $code_handler . '  $export[\'' . $name . '\'] = $handler;';
    $code[] = '';

  }

  $code[] = '  return $export;';
  $code = implode("\n", $code);
  return array('transform_page_manager_handlers' => $code);
}

function pane_reuse_page_manager_handlers_features_revert($module_name) {
  $handlers = module_invoke($module_name, 'transform_page_manager_handlers');

  if (!empty($handlers)) {
    foreach ($handlers as $name => $handler) {
      page_manager_delete_task_handler($handler);
    }
  }
}

function pane_reuse_page_manager_handlers_features_rebuild($module_name) {

  $mycomponents = module_invoke($module_name, 'transform_page_manager_handlers');

  if (!empty($mycomponents)) {
    foreach ($mycomponents as $name => $mycomponent) {
      page_manager_save_task_handler($mycomponent);
    }
  }
}