<?php
/**
 * @file
 * Code for the Pane reuse example feature.
 */

include_once 'pane_reuse_example.features.inc';

/**
 * implements hook_pane_reuse_page_manager_handlers().
 */
function pane_reuse_example_pane_reuse_page_manager_handlers() {
  $transform = array();

  $transform = array_merge(
    $transform,
    pane_reuse_example_node_view_panel_context_existing(),
    pane_reuse_example_node_view_panel_context_my_2(),
    pane_reuse_example_node_view_panel_context_rename_existing()
      // move - copy and replace,add
      // then verify alter3,4
  );

  return $transform;

}

/**
 * Flip two panes (no need to backup because there is cloning of the pane)
 */
function pane_reuse_example_node_view_panel_context_rename_existing() {
  $transform = array();

  $handler = new stdClass();

  $source_handler_name = 'node_view_panel_context_existing';
  $handler->name = 'node_view_panel_context_rename_existing';

  $handler->task = 'node_view';

  $handler->conf['title'] = 'Node 4 (flipping)';

  $handler->conf['access'] = array(
      'plugins' => array(
          0 => array(
              'name' => 'php',
              'settings' => array(
                  'description' => '',
                  'php' => 'return $contexts[\'argument_entity_id:node_1\']->data->uuid == \'d90032eb-8fc5-47a2-9c42-4bd49eb54f96\';',
              ),
              'not' => FALSE,
          ),
      ),
      'logic' => 'and',
  );

  $get_panes_by_pos = array(
    $handler->name => array(
      'right' => array(
        '0' => array(
          'type' => 'custom',
          'subtype' => 'custom',
        ),
        '1' => array(
            'type' => 'custom',
            'subtype' => 'custom',
        ),
      )),
  );

  $trans_panes_by_pos = array(
    $handler->name => array(
      'right' => array(
        0 => array(
          'position' => '1'

        ),
        1 => array(
          'position' => '0',
        ),
      )),
  );

  $transform[$handler->name] = array($source_handler_name, $handler, $get_panes_by_pos, $trans_panes_by_pos);

  return $transform;
}

/**
 * Transform existing, test $actual andd $actual_dest
 *
 */
function pane_reuse_example_node_view_panel_context_existing() {
  $transform = array();

  $handler = new stdClass();

  $source_handler_name = 'node_view_panel_context';
  $handler->name = 'node_view_panel_context_existing';
  $handler->task = 'node_view';

  $handler->conf['title'] = 'Node 3 (existing handler) transformed';

  $handler->conf['access'] = array(
      'plugins' => array(
          0 => array(
              'name' => 'php',
              'settings' => array(
                  'description' => '',
                  'php' => 'return $contexts[\'argument_entity_id:node_1\']->data->uuid == \'7c5c39c0-dec5-410f-98f1-53631a83c3f7\';',
              ),
              'not' => FALSE,
          ),
      ),
      'logic' => 'and',
  );

  $get_panes_by_pos = array(
    'node_view_panel_context' => array(
      'right' => array(
        '0' => array(
          'type' => 'custom',
          'subtype' => 'custom',
          'pid' => 'new-2', // test with new-4 $actual_dest (new-9) ;
      ),

      '1' => array(
        'type' => 'custom',
        'subtype' => 'custom',
        // test no pid, so $actual
      ),
      )),


  );

  // @depends $get_panes_by_pos
  $trans_panes_by_pos = array(
    'node_view_panel_context' => array(
      'right' => array(
        '0' => array(
          'configuration' => array('body' => 'transformed right/0 node 1->2'),
        ),
        '1' => array(
          'configuration' => array('body' => 'transformed right/1 node 1->2'),
        ))),
  );

  $transform[$handler->name] = array($source_handler_name, $handler, $get_panes_by_pos, $trans_panes_by_pos);

  return $transform;
}

/**
 * Add new handler (display is copied likewise ...->panels and ...->content) then:
 * Copy and transform pane from node_view_panel_context/right/0
 * Add new pane (reserve place from node_view_panel_context_my_2/right/1, at $pane->panel and $pane->position)
 * Delete position by node_view_panel_context/right/2 in $handler->name (node_view_panel_context_my_2)
 */
function pane_reuse_example_node_view_panel_context_my_2() {
  $transform = array();

  $handler = new stdClass();

  $source_handler_name = 'node_view_panel_context';
  $handler->name = 'node_view_panel_context_my_2';
  $handler->task = 'node_view';

  $handler->conf['title'] = 'Node 2';

  $handler->conf['access'] = array(
      'plugins' => array(
          0 => array(
              'name' => 'php',
              'settings' => array(
                  'description' => '',
                  'php' => 'return $contexts[\'argument_entity_id:node_1\']->data->uuid == \'7a03be07-9d1a-4e19-97b4-5382f0409ebf\';',
              ),
              'not' => FALSE,
          ),
      ),
      'logic' => 'and',
  );

  $get_panes_by_pos = array(
      'node_view_panel_context' => array(
          'right' => array('0' => array(
              'type' => 'custom',
              'subtype' => 'custom',
          ))),
  );

  $trans_panes_by_pos = array(
      'node_view_panel_context' => array(
          'right' => array('0' => array(
              'pid' => 'no-conflict',
              'configuration' => array('title' => 'Adding title'),
          ))),
  );

  // New pane to the left
  $pane = new stdClass();
  $pane->pid = 'new-6';
  $pane->panel = 'right';
  $pane->type = 'custom';
  $pane->subtype = 'custom';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
      'admin_title' => '',
      'title' => '',
      'body' => 'custom new content for node 2',
      'format' => 'filtered_html',
      'substitute' => 1,
  );
  $pane->cache = array();
  $pane->style = array(
      'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();

  // Reserve the place by = array();
  $get_panes_by_pos['node_view_panel_context']['right']['1'] = array();
  $trans_panes_by_pos['node_view_panel_context']['right']['1'] = $pane;

  // Delete position 2 (copied from node_view_panel_context when copying the display)
  $get_panes_by_pos['node_view_panel_context']['right']['2'] = array();

  $transform[$handler->name] = array($source_handler_name, $handler, $get_panes_by_pos, $trans_panes_by_pos);

  return $transform;
}
